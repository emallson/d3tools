#include "stdafx.h"
#include "../libd3/Affix.h"
#include "test_data.h"
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;

using namespace Diablo3;

namespace d3test
{
	[TestClass]
	public ref class AffixTest
	{
	public: 
		[TestMethod]
		void testAffixConstuctors()
		{
			string type = "Intelligence_Item";
			Affix test(type,TEST_AFFIX_JSON);
			Assert::IsTrue(test.getType() == type);
			Assert::AreEqual((double)85, test.getMin());
			Assert::AreEqual((double)85, test.getMax());

			Affix copied(test);
			Assert::IsTrue(copied.getType() == type);
			Assert::AreEqual((double)85, copied.getMin());
			Assert::AreEqual((double)85, copied.getMax());
		}
	};
}
