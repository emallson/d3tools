#include "stdafx.h"
#include "test_data.h"

#include "../libd3/Item.h"

using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;

using namespace Diablo3;

namespace d3test
{
	[TestClass]
	public ref class ItemTest
	{
	public: 
		[TestMethod]
		void testItemConstructors()
		{
			Item test(TEST_ITEM_JSON);
			Assert::AreEqual((unsigned short)52, test.getIlvl());
			Assert::AreEqual((unsigned short)51, test.getReqLvl());
			Assert::AreEqual((unsigned short)11, test.getAffixes().size());
			
			Item test2(test);
			Assert::AreEqual((unsigned short)52, test2.getIlvl());
			Assert::AreEqual((unsigned short)51, test2.getReqLvl());
			Assert::AreEqual((unsigned short)11, test2.getAffixes().size());
		}
	};
}
