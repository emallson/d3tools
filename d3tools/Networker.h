#pragma once

#include <boost/asio.hpp>
#include <boost/property_tree/ptree.hpp>

using namespace std;
using namespace boost::asio;
using boost::asio::ip::tcp;
using boost::property_tree::ptree;

#define BATTLE_NET "us.battle.net"
#define _WIN32_WINNT 0x0501

class Networker
{
public:
	Networker(void);
	~Networker(void);

	ptree load_json(string url);

	bool open();
	bool close();

	enum NetworkerErrors {
		ERROR_BATTLE_NET_UNREACHABLE,
	};

private:
	io_service _ios;
	tcp::socket * _sock;
};