#include "Networker.h"

#include <boost/array.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/regex.hpp>

using namespace std;
using namespace boost::asio;
using boost::asio::ip::tcp;
using boost::property_tree::ptree;

Networker::Networker(void)
{
	_sock = new tcp::socket(_ios); // initialize the socket
}


Networker::~Networker(void)
{
	close();

	// finish the cleanup, delete the pointers
	delete _sock;
}

bool Networker::open() {

	if(_sock->is_open()) {
		return true;
	}

	try {
		tcp::resolver res(_ios); // set up the network resolver
		tcp::resolver::query query(BATTLE_NET,"http"); // prepare the query
		tcp::resolver::iterator it = res.resolve(query); // query the server
		connect(*_sock,it); // attempt a connection
	} catch(boost::system::system_error &e) {
		cerr << e.what() << endl;
		return false;
	}

	return _sock->is_open();
}

bool Networker::close() {
	boost::system::error_code error; // var to hold any errors encountered when closing

	// only close if the socket is open
	if(_sock->is_open()) {
		_sock->close(error);
	}

	// if an error occured, send it to cerr
	if(error) {
		cerr << error.message() << endl;
		return false;
	}

	return true;
}

// NOTE: The URL *must* match the format: /api/d3/<remainder of url>
// If it doesn't match, you're gonna have a bad time
ptree Networker::load_json(string url) {

	// set up our data variables
	ptree data;
	boost::system::error_code error;
	boost::array<char,128> buff;

	// construct the message
	stringstream msg_stream;
	msg_stream << "GET " << url << " HTTP/1.0\n";
	msg_stream << "Host: " << BATTLE_NET << "\n";
	msg_stream << "Accept: */*\n";
	msg_stream << "Connection: close\n\n";
	string response = "";

	open(); // open the connection

	// write the message
	write(*_sock,buffer(msg_stream.str()),error);

	// if we have an error, send it out
	if(error) {
		cerr << error.message() << endl;
	}
	
	// while there is not an error, read the data into the response string
	size_t len = 0;
	while(len = _sock->read_some(boost::asio::buffer(buff),error)) {
		for(unsigned int i = 0; i < len; i++) {
			response += buff[i];
		}
	}
	// if there is an error, send it out
	// exception is the EOF 'error', which simply signifies 
	// that we reached the end of the file
	if(error && error.value() != 2) {
		cerr << error.value() << ": " << error.message() << endl;
	}

	close(); // close the connection

	for(int i = 0; i < response.length(); i++) {
		if(response[i] == '{') {
			response = response.substr(i);
			break;
		}
	}
	stringstream ss;
	ss << response;
	try {
		boost::property_tree::read_json(ss,data);
	} catch(boost::property_tree::json_parser_error &e) {
		cerr << e.what() << endl << ss.str() << endl;
		return data;
	}

	return data;
}