#include "Networker.h"
#include "../libd3/Item.h"
#include "../libd3/Career.h"

#include <boost/regex.hpp>

int main(int argc, char* argv[]) {

	if(argc != 2) {
		cout << "Usage: " << argv[0] << " <uri>" << endl;
		return EXIT_FAILURE;
	}

	Networker n;
	try {
		boost::regex career_regex("^/api/d3/profile/[\\w\\d]{3,12}-\\d{1,4}/$");
		boost::regex hero_regex("^/api/d3/profile/[\\w\\d]{3,12}-\\d{1,4}/hero/\\d+$");
		boost::regex item_regex("^/api/d3/data/item/[^/\\n]+$");
		if(boost::regex_match(argv[1],career_regex)) {
			Diablo3::Career career(n.load_json(argv[1]));
			cout << career.getRaw<string>("battleTag") << endl;
		} else if(boost::regex_match(argv[1],hero_regex)) {
			Diablo3::Hero hero(n.load_json(argv[1]));
			cout << hero.getRaw<string>("name") << endl;
		} else if(boost::regex_match(argv[1],item_regex)) {
			Diablo3::Item item(n.load_json(argv[1]));
			cout << item.getRaw<string>("name") << endl;
		} else {
			return EXIT_FAILURE;
		}
	} catch(boost::regex_error& e) {
		cout << e.what() << endl;
		return EXIT_FAILURE;
	} catch(std::exception& e) {
		cout << e.what() << endl;
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}