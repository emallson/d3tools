#include "stdafx.h"
#include "Hero.h"

namespace Diablo3 {

	Hero::Hero(string json) : PtreeContainerBase(json) {
		readItemIds();
	}

	Hero::Hero(ptree& data) : PtreeContainerBase(data) {
		readItemIds();
	}

	Hero::Hero(const Hero& from) : PtreeContainerBase(from) {
		for(vector<const Item>::const_iterator i = from._items.begin();
			i != from._items.end(); i++) {
				_items.push_back(*i);
		}
	}

	Hero& Hero::operator=(const Hero& from) {
		(PtreeContainerBase)*this = (PtreeContainerBase)from;
		for(vector<const Item>::const_iterator i = from._items.begin();
			i != from._items.end(); i++) {
				_items.push_back(*i);
		}
		return *this;
	}

	const vector<const Item>& Hero::getItems() const {
		return _items;
	}

	const vector< pair<string,string> >& Hero::getItemParams() const {
		return _itemParams;
	}

	// Attempts to read in the paramters for items (slot and tooltipParams)
	// Returns true if successful, false if there was a problem (ex: path not existing)
	bool Hero::readItemIds() {
		try {
			ptree items = _pt.get_child("items");
			for(ptree::const_iterator i = items.begin(); i != items.end(); i++) {
				_itemParams.push_back( pair<string,string>(i->first,i->second.get<string>("tooltipParams")) ); // slot, params
			}

			return true;

		} catch(const boost::property_tree::ptree_bad_path &error) {
			cerr << error.what() << endl;
			return false;
		}
	}

	// Attempts to add an Item to the Hero. 
	// Returns true if the operation is successful,
	// returns false if the Item doesn't belong to this Hero or is invalid.
	bool Hero::addItemData(ptree& data) {
		string params = data.get<string>("tooltipParams","");

		if(params == "") {
			return false;
		}

		for(vector< pair<string,string> >::const_iterator i = _itemParams.begin();
			i != _itemParams.end(); i++) {
				if(i->second == params) {
					_items.push_back(Item(data,i->first));
					return true;
				}
		}

		return false;

	}

};
