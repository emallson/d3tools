#pragma once

#include "common.h"
#include "PtreeContainerBase.h"
#include "Hero.h"

using namespace std;

namespace Diablo3 {

	class LIBD3_API Career : public PtreeContainerBase
	{
	public:
		Career(string json);
		Career(const Career&);
		Career(ptree& data);

		Career& operator=(const Career&);

		const vector<const Hero>& getHeroes() const;
		const vector<int>& getHeroIds() const;

		bool addHeroData(ptree&);

	private:
		vector<const Hero> _heroes;
		vector<int> _heroIds;
		bool readHeroIds();
	};

};
