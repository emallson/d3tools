#include "stdafx.h"
#include "PtreeContainerBase.h"

using namespace std;
using namespace boost::property_tree;

namespace Diablo3 {

	// Attempts to read the provided JSON into the property tree.
	// If it fails, it throws an std::exception with the error code ERROR_INVALID_JSON.
	PtreeContainerBase::PtreeContainerBase(string json = "") : _rawJSON(json)
	{
		if(!ptreeFromJSON(_rawJSON)) {
			std::exception e("Invalid JSON",ERROR_INVALID_JSON);
			throw e;
		}
	}

	// Attempts to copy the data from another PtreeContainerBase
	// If it fails, it throws an std::exception with the error code ERROR_INVALID_JSON.
	PtreeContainerBase::PtreeContainerBase(const PtreeContainerBase& from) 
		: _rawJSON(from._rawJSON), _pt(from._pt) {
	}

	PtreeContainerBase::PtreeContainerBase(ptree& data) : _rawJSON(""), _pt(data) {}

	// Attempts to get a child ptree from the internal ptree
	// If no such child exists, boost::property_tree::ptree_bad_path is thrown.
	const ptree& PtreeContainerBase::getChild(string path) const {
		return _pt.get_child(path);
	}

	// Attempts to read the given json into a ptree.
	// Returns true if the operation is successful, otherwise false.
	bool PtreeContainerBase::ptreeFromJSON(string json) {
		try {
			stringstream ss;
			ss << json;
			read_json(ss, _pt);
		} catch(const json_parser_error &error) {
			cerr << error.message() << endl;
			return false;
		} catch(const ptree_bad_data &error) {
			cerr << error.what() << endl;
			return false;
		}

		return true;
	}

	// Attempts to assign this PtreeContainerBase the values of another.
	// If there is no raw JSON or the raw JSON is invalid, a direct copy is done.
	// This may or may not be safe. Further testing is needed.
	// TODO: test ptree copying
	PtreeContainerBase& PtreeContainerBase::operator=(const PtreeContainerBase& from) {
		_rawJSON = from._rawJSON;
		if(!ptreeFromJSON(_rawJSON)) {
			_pt = from._pt; // we will see what happens with this
		}
		return *this;
	}

};
