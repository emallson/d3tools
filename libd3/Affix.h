#pragma once

#include "common.h"
#include "PtreeContainerBase.h"

#include<vector>
#include <boost/property_tree/json_parser.hpp>

using namespace std;
using boost::property_tree::ptree;

namespace Diablo3 {

	class LIBD3_API Affix : PtreeContainerBase
	{
	public:
		// Constructors
		// Initializes with values from another Affix
		Affix(const Affix&);
		// Initializes with data from a parsed JSON object (ptree)
		// the type is already set as a string
		Affix(string,ptree&);
		Affix(string,string);

		Affix& operator=(const Affix&);

		// Getters & Setters
		string getType() const;
		double getMin() const;
		double getMax() const;

	private:
		// the type of affix
		string _type;

	};

};
