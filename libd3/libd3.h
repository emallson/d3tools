// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the LIBD3_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// LIBD3_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef LIBD3_EXPORTS
#	define LIBD3_API __declspec(dllexport)
#	define EXPIMP_TEMPLATE
#else
#	define LIBD3_API __declspec(dllimport)
#	define EXPIMP_TEMPLATE extern
#endif

// Equivalent to -D_SCL_SECURE_NO_WARNINGS
#pragma warning ( disable: 4996 )
#pragma warning ( disable: 4251 )

#include <string>
#include <vector>
#include <boost/property_tree/ptree.hpp>