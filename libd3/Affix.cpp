#include "stdafx.h"
#include "Affix.h"

namespace Diablo3 {

	Affix::Affix(const Affix& from) 
		: _type(from._type), PtreeContainerBase(from) {}

	Affix::Affix(string type, ptree &json) : _type(type), 
		PtreeContainerBase(json) {}

	Affix::Affix(string type, string json) : _type(type), PtreeContainerBase(json) {}

	string Affix::getType() const {
		return _type;
	}

	double Affix::getMax() const {
		return _pt.get<double>("max");
	}

	double Affix::getMin() const {
		return _pt.get<double>("min");
	}

	Affix& Affix::operator=(const Affix& from) {
		(PtreeContainerBase)*this = (PtreeContainerBase)from;
		_type = from._type;
		return *this;
	}

};