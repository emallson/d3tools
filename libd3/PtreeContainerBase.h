#pragma once

#include "common.h"

#include<boost/property_tree/json_parser.hpp>

using namespace std;
using boost::property_tree::ptree;


namespace Diablo3 {

	// This class is used to consolidate code from several different classes
	// concerning the loading and storage of property trees.
	//
	// It is not intended to be used directly.
	class LIBD3_API PtreeContainerBase
	{
	public:
		PtreeContainerBase(string json);
		PtreeContainerBase(const PtreeContainerBase&);
		PtreeContainerBase(ptree& data);

		PtreeContainerBase& operator=(const PtreeContainerBase&);

		template<typename T> T getRaw(string path) const {
			return _pt.get<T>(path);
		};

		const ptree& getChild(string path) const;

		virtual bool ptreeFromJSON(string);

		enum PtreeContainerBaseErrors {
			ERROR_INVALID_JSON,
		};

	protected:
		string _rawJSON;
		ptree _pt;
	};

};