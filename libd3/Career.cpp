#include "stdafx.h"
#include "Career.h"


namespace Diablo3 {

	Career::Career(string json) : PtreeContainerBase(json) {
		readHeroIds();
	}

	Career::Career(const Career& from) : PtreeContainerBase(from) {
		for(vector<const Hero>::const_iterator i = from._heroes.begin();
			i != from._heroes.end(); i++) {
				_heroes.push_back(*i);
		}
	}

	Career::Career(ptree& data) : PtreeContainerBase(data) {
		readHeroIds();
	}

	Career& Career::operator=(const Career& from) {
		(PtreeContainerBase)*this = (PtreeContainerBase)from;

		for(vector<const Hero>::const_iterator i = from._heroes.begin();
			i != from._heroes.end(); i++) {
				_heroes.push_back(*i);
		}
		return *this;
	}

	const vector<const Hero>& Career::getHeroes() const {
		return _heroes;
	}

	const vector<int>& Career::getHeroIds() const {
		return _heroIds;
	}

	// Attempts to read in the IDs for heroes.
	// Returns true if successful, false otherwise.
	bool Career::readHeroIds() {
		try {
			ptree heroes = _pt.get_child("heroes");
			for(ptree::const_iterator i = heroes.begin(); i != heroes.end(); i++) {
				_heroIds.push_back(i->second.get<int>("id"));
			}
			return true;
		} catch(const boost::property_tree::ptree_bad_path &error) {
			cerr << error.what() << endl;
			return false;
		}
	}

	// Attempts to add data for a Hero.
	// Returns true if this Career has the given Hero, returns false  
	// if the data is not a Hero or does not belong to this Career.
	bool Career::addHeroData(ptree& data) {
		int id = data.get<int>("id", Hero::ERROR_MISSING_ID);

		if(std::find(_heroIds.begin(), _heroIds.end(), id) == _heroIds.end()) {
			return false;
		}

		_heroes.push_back(Hero(data));
		return true;
	}

};
