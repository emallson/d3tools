#pragma once

#include "common.h"
#include "PtreeContainerBase.h"

#include<vector>
#include <boost/property_tree/json_parser.hpp>

#include "Affix.h"

using namespace std;
using boost::property_tree::ptree;

namespace Diablo3 {

	class LIBD3_API Item : public PtreeContainerBase
	{
	public:
		Item(string json);
		Item(const Item&);
		Item(ptree& data,string slot = "");

		Item& operator=(const Item&);

		unsigned short getReqLvl() const;
		unsigned short getIlvl() const;
		const vector<const Affix>& getAffixes() const;
		const string& getSlot() const;

		void setSlot(string);

		enum ItemErrors {
			ERROR_ILVL_NOT_EXIST = 65500, // if we ever reach ilvl/reqlevel = 65500, i will eat the keyboard i wrote this with
			ERROR_REQLVL_NOT_EXIST,
		};

	private:
		vector<const Affix> _affixes;
		string _slot;
	};

};