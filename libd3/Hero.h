#pragma once

#include "common.h"
#include "PtreeContainerBase.h"
#include "Item.h"

#include<vector>
#include<boost/property_tree/json_parser.hpp>

using namespace std;
using boost::property_tree::ptree;

namespace Diablo3 {

	class LIBD3_API Hero : public PtreeContainerBase
	{
	public:
		Hero(string json);
		Hero(const Hero&);
		Hero(ptree& data);

		Hero& operator=(const Hero&);

		const vector<const Item>& getItems() const;
		const vector< pair<string,string> >& getItemParams() const;

		bool addItemData(ptree&);

		enum HeroErrors {
			ERROR_MISSING_ID = -100,
		};

	private:
		vector<const Item> _items;
		vector< pair<string,string> > _itemParams;
		bool readItemIds();
	};

};