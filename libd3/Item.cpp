#include "stdafx.h"
#include "Item.h"

#include<sstream>

using namespace std;
using namespace boost::property_tree;

namespace Diablo3 {

	Item::Item(string json = "") : PtreeContainerBase(json) {
		try {
			ptree affixesRaw = _pt.get_child("attributesRaw");
			for(ptree::const_iterator i = affixesRaw.begin(); i != affixesRaw.end(); i++) {
				_affixes.push_back(Affix(i->first,(ptree)i->second));
			}
		} catch(const ptree_bad_path &error) {
			cerr << error.what() << endl;
		}
	}

	Item::Item(const Item& from) : PtreeContainerBase(from) {
		// copy the affixes
		for(vector<Affix>::const_iterator i = from._affixes.begin(); i != from._affixes.end(); i++) {
			_affixes.push_back(Affix(*i));
		}
	}

	Item::Item(ptree& data,string slot) : PtreeContainerBase(data), _slot(slot) {
		try {
			ptree affixesRaw = _pt.get_child("attributesRaw");
			for(ptree::const_iterator i = affixesRaw.begin(); i != affixesRaw.end(); i++) {
				Affix a(i->first,(ptree)i->second);
				_affixes.push_back(a);
			}
		} catch(const ptree_bad_path &error) {
			cerr << error.what() << endl;
		}
	}

	unsigned short Item::getIlvl() const {
			return _pt.get<unsigned short>("itemLevel", ERROR_ILVL_NOT_EXIST);
	}

	unsigned short Item::getReqLvl() const {
			return _pt.get<unsigned short>("requiredLevel", ERROR_REQLVL_NOT_EXIST);
	}

	const vector<const Affix>& Item::getAffixes() const {
		return _affixes;
	}

	const string& Item::getSlot() const {
		return _slot;
	}

	void Item::setSlot(string val) {
		_slot = val;
	}

	// TODO: test affix copying
	Item& Item::operator=(const Item& from) {
		// perform the copy of the base class
		(PtreeContainerBase)*this = (PtreeContainerBase)from;

		// this may or may not be necessary, will test
		for(vector<const Affix>::const_iterator i = from._affixes.begin();
			i != from._affixes.end(); i++) {
				Affix a = *i;
				_affixes.push_back(a);
		}
		return *this;
	}

};
